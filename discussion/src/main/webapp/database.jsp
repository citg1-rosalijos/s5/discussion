<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Database Servlet</title>
	</head>
	<body>
		<!-- Receives data from ServletContext -->
		<% 
			ServletContext servletContext = getServletContext(); 
			String firstname = servletContext.getAttribute("firstname").toString();
			String lastname = servletContext.getAttribute("lastname").toString();
			String email = servletContext.getAttribute("email").toString();
			String contact = servletContext.getAttribute("contact").toString();
		%>
		
		<!-- Prints the retrieve data -->
		<h1>User Information has been stored at the database via JSP.</h1>
		<p>First Name: <%= firstname%></p>
		<p>Last Name: <%= lastname%></p>
		<p>Email: <%= email%></p>
		<p>Contact: <%= contact%></p>
	</body>
</html>