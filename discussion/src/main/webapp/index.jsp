<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.Date" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Java Server Page</title>
	</head>
	<body>
		<h1>Welcome to Hotel Servlet</h1>
		<!-- JSP allows integration of HTML Tags with Java syntax -->
		
		<!-- 
			JSP Scriptlets 
				- allow any Java statements
				- variables declared are placed inside of __jpsService()
		-->
		<% System.out.println("Hello from JSP"); %>
		<%
			serviceVar++;
			System.out.println("__jspService(): service " + serviceVar);
			
			String content1 = "content1: " + initVar;
			String content2 = "content2: " + serviceVar;
			String content3 = "content3: " + destroyVar;
		%>
		
		<!-- 
			JSP Declaration 
				- allows the declaration of one or more variables or methods
				- variable declared are placed outside of __jspService ()
		-->
		<%! Date currentDateTime = new java.util.Date(); %>
		<%!
			private int initVar = 0;
			private int serviceVar = 0;
			private int destroyVar = 0;
		%>
		<%!
			public void jspInit(){
				initVar++;
				System.out.println("jspInit(): init " + initVar);
			}
		
			public void jspDestroy(){
				destroyVar++;
				System.out.println("jspDestroy(): destroy " + destroyVar);
			}
		%>
		
		<!-- 
			JSP Expression 
				- code is written to the output stream of the response
				- out.println is no longer required
		-->
		<p>The time now is <%= currentDateTime %></p>
		<h1>JSP Expression</h1>
		<p><%= content1 %></p>		
		<p><%= content2 %></p>	
		<p><%= content3 %></p>	
		
		<h1>Create an Account</h1>
		<form action="user" method="post">
			<label for="firstname">First Name: </label><br>
			<input name="firstname" type="text"><br>
			
			<label for="lastname">Last Name: </label><br>
			<input name="lastname" type="text"><br>
			
			<label for="email">Email: </label><br>
			<input name="email" type="email"><br>
			
			<label for="contact">Contact Number: </label><br>
			<input name="contact" type="text"><br><br>
			
			<input type="submit">
		</form>		
	</body>
</html>