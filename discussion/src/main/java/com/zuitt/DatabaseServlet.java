package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/database")
public class DatabaseServlet extends HttpServlet {

	private static final long serialVersionUID = -6668827760365993356L;
	// Mock database
	ArrayList<String> users = new ArrayList<>();
	
	public void init() throws ServletException {
		System.out.println("*************************************");
		System.out.println("DatabaseServlet has been initialized.");
		System.out.println("Connected to database. ");
		System.out.println("*************************************");
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		ServletContext servletContext = getServletContext();
		
		String firstname = servletContext.getAttribute("firstname").toString();
		String lastname = servletContext.getAttribute("lastname").toString();
		String email = servletContext.getAttribute("email").toString();
		String contact = servletContext.getAttribute("contact").toString();
		
		// Save the registration
		users.add(firstname + " " + lastname + ": " + email + " " + contact);
		
		out.println(
				"<h1>User information has been stored to the database via servlet.</h1>" + 
				"<p>First Name: " + firstname + "</p>" +
				"<p>Last Name: " + lastname + "</p>" +
				"<p>Email: " + email + "</p>" +
				"<p>Contact Number: " + contact + "</p>" 
			);
	}
	
	public void destroy() {
		System.out.println("***********************************");
		System.out.println("DatabaseServlet has been destroyed.");
		System.out.println("Disconnected to database. ");
		System.out.println("***********************************");
	}

}
