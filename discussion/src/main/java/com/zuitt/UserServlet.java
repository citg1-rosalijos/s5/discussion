package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

// Avoids configurations in web.xml for servlets
@WebServlet("/user")
public class UserServlet extends HttpServlet {

	private static final long serialVersionUID = 7560556849380195433L;
	
	public void init() throws ServletException {
		System.out.println("*********************************");
		System.out.println("UserServlet has been initialized.");
		System.out.println("Connected to database. ");
		System.out.println("*********************************");
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// Capture data via form input
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String email = request.getParameter("email");
		String contact = request.getParameter("contact");
		
		// Set servlet context attributes to be consumed by the DatabaseServlet
		ServletContext servletContext = getServletContext();
		servletContext.setAttribute("firstname", firstname);
		servletContext.setAttribute("lastname", lastname);
		servletContext.setAttribute("email", email);
		servletContext.setAttribute("contact", contact);
		
		// Forward the request and response to the DatabaseServlet
		// response.sendRedirect("database");
		response.sendRedirect("database.jsp");
	}

	public void destroy() {
		System.out.println("*******************************");
		System.out.println("UserServlet has been destroyed.");
		System.out.println("Disconnected to database. ");
		System.out.println("*******************************");
	}
}
